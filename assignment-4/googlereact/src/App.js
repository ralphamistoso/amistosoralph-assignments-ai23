import React, { Component } from 'react';
import './App.css';

import GoogleLogin from 'react-google-login';

class App extends Component {

  render() {

    const responseGoogle = (response) => {
      console.log(response);
    }

    return (
      <div className="App">
        <h1>LOGIN WITH GOOGLE</h1>


      <GoogleLogin
        clientId="627678714890-c00k6l7qnflti3t5cii08fjkhah0s0qs.apps.googleusercontent.com"
        buttonText="LOGIN WITH GOOGLE"
        onSuccess={responseGoogle}
        onFailure={responseGoogle}
      />

      </div>
    );
  }
}

export default App;