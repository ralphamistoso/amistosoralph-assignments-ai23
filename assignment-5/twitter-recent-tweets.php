<?php
session_start();
require 'autoload.php';
use Abraham\TwitterOAuth\TwitterOAuth;

define('CONSUMER_KEY', '5g3jhknwWO8OrMm1Ow48YlrYX'); // add your app consumer key between single quotes
define('CONSUMER_SECRET', 'Xl9I7mobEyZeRVLwjTIKYgqGqav25pgwoZdQRwasDY4Yh2H7TO'); // add your app consumer secret key between single quotes
define('OAUTH_CALLBACK', 'https://localhost/assignment-5/callback.php'); // your app callback URL
if (!isset($_SESSION['access_token'])) {
	$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
	$request_token = $connection->oauth('oauth/request_token', array('oauth_callback' => OAUTH_CALLBACK));
	$_SESSION['oauth_token'] = $request_token['oauth_token'];
	$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
	$url = $connection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));
	echo $url;
} else {
	$access_token = $_SESSION['access_token'];
	$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
	//the basic user info
	$user = $connection->get("account/verify_credentials");
	
	//printing username in the screen
	echo "Welcome  ".$user->screen_name . '<br>';
	
	
	//getting all my recent tweets
	$tweet = $connection->get('statuses/user_timeline', ['count' => 100, 'exclude_replies' => true, 'screen_name' => 'amistoso_ralph', 'include_rts' => false]);
	$totalTweets[] = $tweet;
	$page = 0;

	for($count = 100; $count < 300; $count += 100) {
		$max = count($totalTweets[$page]) - 1;
		$tweet = $connection->get('statuses/user_timeline', ['count' => 100, 'exclude_replies' => true, 'max_id' => $totalTweets[$page][$max]->id_str, 'screen_name' => 'amistoso_ralph', 'include_rts' => false]);
		$totalTweets[] = $tweet;
		$page +=1;
	}

	//for printing recent tweets
	$start = 1;
	foreach ($totalTweets as $page) {
		foreach($page as $key) {
			echo $start . ':' . $key->text . '<br>';
			$start++;
		}
	}
}
