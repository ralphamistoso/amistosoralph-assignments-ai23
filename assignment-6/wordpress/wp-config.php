<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '8n[wb:@|I,xV_`h2^ u^>7M07`~OD4O3!@A]3Q(ln]%UT]J^rPe .xhU -m_W%v>' );
define( 'SECURE_AUTH_KEY',  ';BN3pl<K)N(z|ZC_G&]~cx,oLseiX}w,(%M:k1JAb~]Tp}:$i%>^b#{ZJ/]?-9P;' );
define( 'LOGGED_IN_KEY',    'Xt-7v1sO4JY[p<Wt M;wLzn<`?JN!cI9;1~@HX]F:M3cQckKi8.$!?o_n@$M2_QK' );
define( 'NONCE_KEY',        '|emlWIoE/joSE0*QrT:!,tdOri-oss: 9(Y,zHxFR5?IRB[UEs^Z<)~WQNJ%o&dN' );
define( 'AUTH_SALT',        '+Lrcj>LMaN0?g7jN6e.fGu:j14l|X_?)EWZd>I3 EPJSD1KLyr{FsGxH|3mRL,?!' );
define( 'SECURE_AUTH_SALT', 'j8Zs@0]382@/?V%wU*Nr`_wug#fsgQUc0/<[*tiQIK$Ut7<^tEm =-k@JaR~1V3I' );
define( 'LOGGED_IN_SALT',   '<(-Xa1`~paw{@sB_?VEnQxURS%mTXH{#8qjb)Q9C|WvpWyyc`,ZRanaPXvWk&4(?' );
define( 'NONCE_SALT',       '|]bNG_`6Ke~WMThmPc,gp[K*aA>JZ2)gC95{Aor/,XSX!o8:^{!xCk:=tvHSq#ak' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
